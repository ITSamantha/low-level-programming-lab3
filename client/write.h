#ifndef WRITE_H
#define WRITE_H

#include "data.h"
#include <stdio.h>
#include <stdlib.h>


void printStatement(Statement *statement);

// Function to print the contents of a Value
void printValue(Value* value);

// Function to print the contents of a ColumnInfo
void printColumnInfo(ColumnInfo* columnInfo);

// Function to print the contents of a ColumnDefinition
void printColumnDefinition(ColumnDefinition* columnDefinition);

// Function to print a list of ColumnDefinitions
void printColumnDefinitionList(ColumnDefinitionList* columnDefinitionList);

// Function to print the contents of a ColumnValue
void printColumnValue(ColumnValue* columnValue);

// Function to print a list of ColumnValues
void printColumnValueList(ColumnValueList* columnValueList);

// Function to print the contents of an InsertList
void printInsertList(InsertList* insertList);

// Function to print the contents of a Condition
void printCondition(Condition* condition);

// Function to print the contents of a SelectNewValue
void printSelectNewValue(SelectNewValue* selectNewValue);

// Function to print a list of SelectNewValues
void printSelectValueList(SelectValueList* selectValueList);

// Function to print the contents of a SelectValue
void printSelectValue(SelectValue* selectValue);

// Function to print the contents of a SimpleJoin
void printSimpleJoin(SimpleJoin* simpleJoin);

// Function to print the contents of a SelectStatement
void printSelectStatement(SelectStatement* selectStatement);

// Function to print the contents of an InsertStatement
void printInsertStatement(InsertStatement* insertStatement);

// Function to print the contents of an UpdateStatement
void printUpdateStatement(UpdateStatement* updateStatement);

// Function to print the contents of a CreateStatement
void printCreateStatement(CreateStatement* createStatement);

// Function to print the contents of a DeleteStatement
void printDeleteStatement(DeleteStatement* deleteStatement);

// Function to print the contents of a Statement
void printStatement(Statement* statement);

#endif

