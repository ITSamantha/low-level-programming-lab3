#ifndef DATA_H
#define DATA_H

#include <stdbool.h>

// Enumeration defining various types of LINQ statements
typedef enum {
    STATEMENT_SELECT,
    STATEMENT_INSERT,
    STATEMENT_UPDATE,
    STATEMENT_CREATE,
    STATEMENT_DELETE
} StatementType;

// Enumeration defining different value types 
typedef enum {
    INTEGER_TYPE,
    FLOAT_TYPE,
    STRING_TYPE,
    BOOLEAN_TYPE
} ValueType;

// Enumeration defining different types of conditions 
typedef enum {
    LOGIC_TYPE,
    BINARY_TYPE
} ConditionType;

// Enumeration defining different types for selecting values
typedef enum {
    TABLE_ROW_NAME,
    OTHER_TYPE
} SelectValueType;

// Structure representing a generic value 
typedef struct {
    ValueType valueType;
    union {
        int intValue;
        bool boolValue;
        char *stringValue;
        double floatValue;
    };
} Value;

// Structure representing column information in a table
typedef struct {
    char *tableRowName;
    char *columnName;
} ColumnInfo;

// Structure representing column definition (name and type)
typedef struct {
    char *columnName;
    int columnType;
} ColumnDefinition;

// Linked list of column definitions
typedef struct ColumnDefinitionList {
    ColumnDefinition *columnDefinition;
    struct ColumnDefinitionList *next;
} ColumnDefinitionList;

// Structure representing a column value in an INSERT statement
typedef struct {
    char *columnName;
    Value *value;
} ColumnValue;

// Linked list of column values
typedef struct ColumnValueList {
    ColumnValue *columnValue;
    struct ColumnValueList *next;
} ColumnValueList;

// Linked list of column values for an INSERT statement
typedef struct InsertList {
    ColumnValueList *list;
    struct InsertList *next;
} InsertList;

// Structure representing a condition in a WHERE clause
typedef struct {
    ConditionType conditionType;
    union {
        int logicOperand;
        int binaryOperand;
    } operandType;
    Value *value;
    ColumnInfo *column;
    struct Condition *left;
    struct Condition *right;
} Condition;

// Structure representing a new value in a SELECT statement
typedef struct {
    SelectValueType type;
    union {
        char *newColumnName;
        char *tableRowName;
    } name;
    ColumnInfo *columnInfo;
} SelectNewValue;

// Linked list of new values in a SELECT statement
typedef struct SelectValueList {
    SelectNewValue *value;
    struct SelectValueList *next;
} SelectValueList;

// Structure representing a value in a SELECT statement
typedef struct {
    SelectValueType type;
    union {
        char *tableRowName;
        SelectValueList *list;
    } data;
} SelectValue;

typedef struct {
    char *tableRowName;
    char *tableName;
    ColumnInfo *info1;
    ColumnInfo *info2;
} SimpleJoin;

// Structure representing a simple join in a SELECT statement
typedef struct {
    char *tableRowName;
    SimpleJoin *simpleJoin;
    Condition *condition;
    SelectValue *selectValue;
} SelectStatement;

// Structure representing an INSERT statement
typedef struct {
    InsertList *insertList;
} InsertStatement;

// Structure representing an UPDATE statement
typedef struct {
    ColumnValueList *columnValueList;
    Condition *condition;
} UpdateStatement;

// Structure representing a CREATE TABLE statement
typedef struct {
    ColumnDefinitionList *columnDefinitionList;
} CreateStatement;

// Structure representing a DELETE statement
typedef struct {
    Condition *condition;
} DeleteStatement;

// Union of different statement types
typedef struct {
    union {
        SelectStatement *selectStatement;
        InsertStatement *insertStatement;
        UpdateStatement *updateStatement;
        CreateStatement *createStatement;
        DeleteStatement *deleteStatement;
    } statement;
    StatementType stmtType;
    char *tableName;
} Statement;

// Function to create a new INTEGER Value
Value *createNewIntValue(int intValue);

// Function to create a new BOOLEAN Value
Value *createNewBoolValue(int boolValue);

// Function to create a new STRING Value
Value *createNewStringValue(char *stringValue);

// Function to create a new FLOAT Value
Value *createNewFloatValue(double floatValue);

// Function to create a SimpleJoin structure
SimpleJoin *createSimpleJoin(char *tableRowName, char* tableName, ColumnInfo *info1, ColumnInfo *info2);

// Function to create a new ColumnDefinition
ColumnDefinition *createNewColumnDefinition(char *columnName, int type);

// Function to create a new ColumnValue
ColumnValue *createColumnValue(char *columnName, Value *value);

// Function to create a new ColumnInfo
ColumnInfo *createColumnInfo(char *tableRowName, char *columnName);

// Function to create a new SELECT value with a table row name
SelectValue *createSelectValueWithTableRowName(char *tableRowName);

// Function to create a new SELECT value with a list of new values
SelectValue *createSelectValueWithList(SelectValueList *list);

// Function to create a new SelectNewValue with a table row name
SelectNewValue *createSelectNewValueWithTableRowName(char *tableRowName);

// Function to create a new SelectNewValue with a new column definition
SelectNewValue *createSelectNewValueWithNewDef(char *newColumnName, ColumnInfo *info);

// Function to create a new SelectValueList
SelectValueList *createSelectValueList(SelectNewValue *value, SelectValueList *prev);

// Function to create a new ColumnDefinitionList
ColumnDefinitionList *createColumnDefinitionList(ColumnDefinition *value, ColumnDefinitionList *prev);

// Function to create a new InsertList
InsertList *createInsertList(ColumnValueList *columnValueList, InsertList *prev);

// Function to create a new ColumnValueList
ColumnValueList *createColumnValueList(ColumnValue *value, ColumnValueList *prev);

// Function to create a new binary condition
Condition *createNewBinaryCondition(ColumnInfo *columnInfo, int type, Value *value);

// Function to create a new logic condition
Condition *createNewLogicCondition(int type, Condition *left, Condition *right);

// Function to create a new SELECT statement
Statement *createSelectStatement(char *tableName, char *tableRowName, SimpleJoin *simpleJoin, Condition *condition, SelectValue *value);

// Function to create a new INSERT statement
Statement *createInsertStatement(char *tableName, InsertList *list);

// Function to create a new UPDATE statement
Statement *createUpdateStatement(char *tableName, ColumnValueList *list, Condition *condition);

// Function to create a new CREATE statement
Statement *createCreateStatement(char *tableName, ColumnDefinitionList *list);

// Function to create a new DELETE statement
Statement *createDeleteStatement(char *tableName, Condition *condition);

// Function to free memory associated with a Statement structure
void freeStatement(Statement *statement);

#endif
