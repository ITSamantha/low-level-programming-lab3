#include <stdio.h>
#include "write.h"
#include "parser_linq.tab.h"
#include "lexer_linq.h"
#include "client_interface.h"

statement get_statement(const char* query) {
    statement s;
    yy_scan_string(query);
    yyparse(&s);
    yylex_destroy();
    return s;
}
