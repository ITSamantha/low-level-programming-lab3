%{
#include <stdio.h>
#include "data.h"
#include "write.h"
#define YYERROR_VERBOSE 1
extern int yylex(void);
extern int yylineno;
void yyerror(char const *s);
%}

%define parse.error verbose /* new verison of %error-verbose*/

%token SELECT ADD UPDATE CREATE REMOVE ON IN INTO NEW EQUALS FROM WHERE JOIN
%token STRING INTEGER FLOAT BOOL TYPE
%token LB RB LCB RCB QUOTE SEMICOLON DOT COMMA OTHER EQ
%token COMPARISON CONTAINS

%left OR /* we should use OR or AND Not one token becouse it's has more priority!!! */
%left AND


%type <str> STRING
%type <number> INTEGER
%type <boolType> BOOL
%type <floatValue> FLOAT
%type <binOperand> COMPARISON
%type <binOperand> CONTAINS
%type <valType> TYPE
%type <logicOp> AND
%type <logicOp> OR

%type <statement> statement
%type <statement> createStatement
%type <statement> insertStatement
%type <statement> deleteStatement
%type <statement> updateStatement
%type <statement> selectStatement
%type <simpleJoin> joinStatement
%type <condition> whereStatement
%type <selectValue> selectValue
%type <selectValueList> selectValueList
%type <selectNewValue> selectNewValue
%type <columnDefinitionList> columnDefinitions
%type <columnDefinition> columnDefinition
%type <insertList> insertList
%type <columnValueList> columnValueList
%type <columnValue> columnValue
%type <condition> condition
%type <condition> binaryCondition
%type <value> value
%type <columnInfo> anyColumnName
%type <columnInfo> fullColumnName
%type <str> columnName
%type <str> tableRowName
%type <str> newColumnName
%type <str> tableName

%union {
    char str[32];
    int number;
    int boolType;
    int binOperand;
    int valType;
    int logicOp;
    double floatValue;
    Statement *statement;
    SimpleJoin *simpleJoin;
    SelectValue *selectValue;
    SelectValueList *selectValueList;
    SelectNewValue *selectNewValue;
    ColumnDefinitionList *columnDefinitionList;
    ColumnDefinition *columnDefinition;
    InsertList *insertList;
    ColumnValueList *columnValueList;
    ColumnValue* columnValue;
    Condition *condition;
    Value *value;
    ColumnInfo *columnInfo;
}

%%

input:  %empty /* nothing here */
|	    input statement SEMICOLON {printStatement($2); freeStatement($2); printf("Statement$ ");}
;

statement: 	createStatement
|           insertStatement
|           deleteStatement
|           updateStatement
|           selectStatement
;

createStatement:    tableName DOT CREATE LB columnDefinitions RB {$$ = createCreateStatement($1, $5);}
;

insertStatement:    tableName DOT ADD LB insertList RB {$$ = createInsertStatement($1, $5);}
;

deleteStatement:    tableName DOT REMOVE LB RB {$$ = createDeleteStatement($1, NULL);}
|                   tableName DOT REMOVE LB RB DOT WHERE LB condition RB {$$ = createDeleteStatement($1, $9);}
;

updateStatement:    tableName DOT UPDATE LB columnValueList RB {$$ = createUpdateStatement($1, $5, NULL);}
|                   tableName DOT UPDATE LB columnValueList RB DOT WHERE LB condition RB {$$ = createUpdateStatement($1, $5, $10);}
;

selectStatement:    FROM tableRowName IN tableName joinStatement whereStatement SELECT selectValue {$$ = createSelectStatement($4, $2, $5, $6, $8);}
;

joinStatement:  %empty {$$ = NULL;}
|               JOIN tableRowName IN tableName ON fullColumnName EQUALS fullColumnName {$$ = createSimpleJoin($2, $4, $6, $8);}
;

whereStatement: %empty {$$ = NULL;}
|               WHERE condition {$$ = $2;}
;

selectValue:    tableRowName {$$ = createSelectValueWithTableRowName($1);}
|               NEW LCB selectValueList RCB {$$ = createSelectValueWithList($3);}
;

selectValueList:    selectNewValue COMMA selectValueList {$$ = createSelectValueList($1, $3);}
|                   selectNewValue {$$ = createSelectValueList($1, NULL);}
;

selectNewValue:     newColumnName EQ fullColumnName {$$ = createSelectNewValueWithNewDef($1, $3);}
|                   tableRowName {$$ = createSelectNewValueWithTableRowName($1);}
;

columnDefinitions:  columnDefinition COMMA columnDefinitions {$$ = createColumnDefinitionList($1, $3);}
|                   columnDefinition {$$ = createColumnDefinitionList($1, NULL);}
;

columnDefinition:   columnName TYPE {$$ = createNewColumnDefinition($1, $2);}
;

insertList: NEW LCB columnValueList RCB COMMA insertList {$$ = createInsertList($3, $6);}
|           NEW LCB columnValueList RCB {$$ = createInsertList($3, NULL);}
;

columnValueList:    columnValue COMMA columnValueList {$$ = createColumnValueList($1, $3);}
|                   columnValue {$$ = createColumnValueList($1, NULL);}
;

columnValue:    columnName EQ value {$$ = createColumnValue($1, $3);}
;

condition:  condition OR condition  /* in theory it's better to double condition for method and select options! */ {$$ = createNewLogicCondition($2, $1, $3);}
|           condition AND condition {$$ = createNewLogicCondition($2, $1, $3);}
|           binaryCondition
;

binaryCondition:  anyColumnName COMPARISON value {$$ = createNewBinaryCondition($1, $2, $3);}
|                 anyColumnName CONTAINS QUOTE STRING QUOTE {$$ = createNewBinaryCondition($1, $2, createNewStringValue($4));}
;


value:      INTEGER {$$ = createNewIntValue($1);}
|           FLOAT {$$ = createNewFloatValue($1);}
|           BOOL {$$ = createNewBoolValue($1);}
|           QUOTE STRING QUOTE {$$ = createNewStringValue($2);}
;


anyColumnName:  fullColumnName
|               columnName {$$ = createColumnInfo(NULL, $1);}
;

fullColumnName: tableRowName DOT columnName {$$ = createColumnInfo($1, $3);}
;

columnName: STRING
;

tableRowName: STRING
;

newColumnName: STRING
;

tableName: STRING /* By default $$ = $1 */
;

%%

void yyerror (char const *s) {
   fprintf (stderr, "got %s on line number %d\n", s, yylineno);
}

int main() {
	printf("Statement$ ");
	yyparse();
	return 0;
}
