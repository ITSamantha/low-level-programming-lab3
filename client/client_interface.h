
#include "data.h"

#ifndef LLP_LAB3_CLIENT_INTERFACE_H
#define LLP_LAB3_CLIENT_INTERFACE_H

#include "write.h"

statement get_statement(const char* query);

#endif //LLP_LAB3_CLIENT_INTERFACE_H
