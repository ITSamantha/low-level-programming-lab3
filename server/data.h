#ifndef LLP_LAB1_DATA_H
#define LLP_LAB1_DATA_H

typedef struct data data;
typedef struct dataHeader dataHeader;

#include "table.h"

struct __attribute__((packed)) dataHeader {
    bool is_valid;
    int32_t nextInvalidRowPage;
    int32_t nextInvalidRowOrdinal;
};

struct data {
    dataHeader* dataHeader;
    table* table;
    void** bytes;
    uint16_t pointer;
};

// Initialize data for database
data* initialize_data(table* tb);
//Initialize integer data
void initialize_integer_data(data* dt, int32_t val, char* columnName);
//Initialize string data
void initialize_string_data(data* dt, char* val, char* columnName);
//Initialize bool data
void initialize_bool_data(data* dt, bool val, char* columnName);
//Initialize float data
void initialize_float_data(data* dt, double val, char* columnName);
// Initialize data for database. Depends on type.
void initialize_custom_data(data* dt, columnType type, void* val, char* columnName);
//Try to insert data
bool insert_data(data* data, database* db);

#endif //LLP_LAB1_DATA_H
