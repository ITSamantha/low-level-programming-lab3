#ifndef LLP_LAB1_DATA_ITERATOR_H
#define LLP_LAB1_DATA_ITERATOR_H

#define NONE_OFFSET_TO_COLUMN_DATA -1

typedef struct data_iterator data_iterator;

#include "database.h"
#include "data.h"

struct data_iterator {
    database* db;
    table* tb;
    page* currentPage;
    uint16_t pointer;
    uint16_t rowsReadOnPage;
};

data_iterator* initialize_iterator(database* db, table* tb);
int32_t get_offset_to_column_data(data_iterator* iter, const char* columnName, columnType colType);
bool has_next(data_iterator* iter);
bool seek_next(data_iterator* iter);
bool seek_next_predicate(data_iterator* iter, predicate_T* pred);
bool seek_next_where(data_iterator* iter, const char* colName, columnType colType, const void* val);
uint16_t delete_where(database* db, table*tb, const char* colName, columnType colType, const void* val);
uint16_t delete_where_predicate(database* db, table* tb, predicate_T* pred);
columnType discover_column_type_by_name(data_iterator* iter, const char* name);
uint16_t update_where(database* db, table* tb,
                     const char* whereColName, columnType whereColType, const void* whereVal,
                     const char* updateColName, columnType updateColType, const void* updateVal);
uint16_t update_where_predicate(database* db, table* tb, predicate_T* pred, GPtrArray * sets);
dataHeader get_data_header(data_iterator* iter);
bool get_integer(data_iterator* iter, const char* columnName, int32_t* dest);
bool get_string(data_iterator* iter, const char* columnName, char** dest);
bool get_bool(data_iterator* iter, const char* columnName, bool* dest);
bool get_float(data_iterator* iter, const char* columnName, double* dest);
void print_join_table(database* db, table* tb1, table* tb2, const char* onColumnT1, const char* onColumnT2, columnType type);

#endif // LLP_LAB1_DATA_ITERATOR_H