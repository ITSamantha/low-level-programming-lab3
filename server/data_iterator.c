#include <glib-2.0/glib.h>
#include <glib-2.0/glib-object.h>
#include "data_iterator.h"
#include "../gen-c_glib/structs_types.h"

#define DECLARE_NUMBER_COMPARE_FUNCTION(Type, name) \
bool compare_nums_##name (comparison comp, const Type* arg1, const Type* arg2) { \
    switch (comp) { \
        case GREATER: \
            return *arg1 > *arg2; \
        case LESS: \
            return *arg1 < *arg2; \
        case NEQUAL: \
            return *arg1 != *arg2; \
        case EQUAL: \
            return *arg1 == *arg2; \
        case GREATER_OR_EQUAL: \
            return *arg1 >= *arg2; \
        case LESS_OR_EQUAL: \
            return *arg1 <= *arg2; \
        default: \
            return false; \
    } \
}

enum binop {
    AND = 1,
    OR = 2
};

enum comparison {
    GREATER = 1,
    LESS = 2,
    NEQUAL = 3,
    EQUAL = 4,
    GREATER_OR_EQUAL = 5,
    LESS_OR_EQUAL = 6
};

union value {
    int32_t colInt;
    char** colChar;
    bool colBool;
    double colFloat;
};

typedef enum comparison comparison;
typedef union value value;


data_iterator* initialize_iterator(database* db, table* tb) {
    data_iterator* iter = malloc(sizeof (data_iterator));
    iter->tb = tb;
    iter->db = db;
    iter->currentPage = tb->firstPage;
    iter->pointer = 0;
    iter->rowsReadOnPage = 0;
    return iter;
}

int32_t get_offset_to_column_data(data_iterator* iter, const char* columnName, columnType colType) {
    int32_t offsetToColumnData = sizeof(dataHeader);
    bool match = false;
    for (int i = 0; i < iter->tb->header->columnAmount; i++) {
        columnHeader currentColumn = iter->tb->header->columns[i];
        if (currentColumn.type == colType && strcmp(columnName, currentColumn.columnName) == 0) {
            match = true;
            break;
        }
        offsetToColumnData += currentColumn.size;
    }
    if (!match) return NONE_OFFSET_TO_COLUMN_DATA;
    return offsetToColumnData;
}

bool has_next(data_iterator* iter) {
    return iter->pointer + iter->tb->header->oneRowSize <= DEFAULT_PAGE_SIZE - sizeof(tableHeader) - sizeof(pageHeader)
    && iter->rowsReadOnPage < iter->currentPage->header->rowsAmount;
}

bool seek_next(data_iterator* iter) {
    dataHeader dHeader = {false};
    uint32_t nextPageNumber = iter->currentPage->header->nextPageOrdinal;
    while (has_next(iter) || nextPageNumber != 0) {
        do {
            if (iter->rowsReadOnPage > 0) {
                iter->pointer += iter->tb->header->oneRowSize;
            }
            dHeader = get_data_header(iter);
            iter->rowsReadOnPage += 1;
        } while (has_next(iter) && !dHeader.is_valid);

        if (dHeader.is_valid) {
            return true;
        }

        if (iter->currentPage != iter->tb->firstPage && iter->currentPage != iter->tb->firstAvailableForWritePage) {
            close_page(iter->currentPage);
        }
        if (nextPageNumber == 0) {
            break;
        }
        iter->currentPage = read_page(iter->db, nextPageNumber);
        nextPageNumber = iter->currentPage->header->nextPageOrdinal;
        iter->rowsReadOnPage = 0;
        iter->pointer = 0;
    }
    return false;
}

bool seek_next_where(data_iterator* iter, const char* colName, columnType colType, const void* val) {
    dataHeader dHeader = {false};

    int32_t colInt;
    char* colChar;
    bool colBool;
    double colFloat;
    uint32_t nextPageNumber = iter->currentPage->header->nextPageOrdinal;
    while (has_next(iter) || nextPageNumber != 0) {
        do {
            if (iter->rowsReadOnPage > 0) {
                iter->pointer += iter->tb->header->oneRowSize;
            }
            dHeader = get_data_header(iter);
            iter->rowsReadOnPage += 1;
        } while (has_next(iter) && !dHeader.is_valid);

        if (dHeader.is_valid) {
            switch (colType) {
                case INTEGER:
                    get_integer(iter, colName, &colInt);
                    if (colInt == *((int32_t *) val)) {
                        return true;
                    }
                    continue;
                case STRING:
                    colChar = malloc(sizeof(char) * DEFAULT_STRING_LENGTH);
                    get_string(iter, colName, &colChar);
                    if (strcmp(colChar, *((char**) val)) == 0) {
                        free(colChar);
                        return true;
                    }
                    free(colChar);
                    continue;
                case BOOLEAN:
                    get_bool(iter, colName, &colBool);
                    if (colBool == *((bool *) val)) {
                        return true;
                    }
                    continue;
                case FLOAT:
                    get_float(iter, colName, &colFloat);
                    if (colFloat == *((double *) val)) {
                        return true;
                    }
                    continue;
                default:
                    break;
            }
        }

        if (iter->currentPage != iter->tb->firstPage && iter->currentPage != iter->tb->firstAvailableForWritePage) {
            close_page(iter->currentPage);
        }
        if (nextPageNumber == 0) {
           break;
        }
        iter->currentPage = read_page(iter->db, nextPageNumber);
        nextPageNumber = iter->currentPage->header->nextPageOrdinal;
        iter->rowsReadOnPage = 0;
        iter->pointer = 0;
    }
    return false;
}

columnType discover_column_type_by_name(data_iterator* iter, const char* name) {
    for (size_t i = 0; i < iter->tb->header->columnAmount; i++) {
        if (strcmp(name, iter->tb->header->columns[i].columnName) == 0) {
            return iter->tb->header->columns[i].type;
        }
    }
}

columnType extract_column_value(data_iterator* iter, columnref_T* col, value* val) {
    columnType type = discover_column_type_by_name(iter, col->col_name);
    switch (type) {
        case INTEGER:
            get_integer(iter, col->col_name, &val->colInt);
            break;
        case FLOAT:
            get_float(iter, col->col_name, &val->colFloat);
            break;
        case STRING:
            get_string(iter, col->col_name, val->colChar);
            break;
        case BOOLEAN:
            get_bool(iter, col->col_name, &val->colBool);
        default:
            break;
    }
    return type;
}

DECLARE_NUMBER_COMPARE_FUNCTION(int32_t, int)

DECLARE_NUMBER_COMPARE_FUNCTION(bool, bool)

DECLARE_NUMBER_COMPARE_FUNCTION(double, float)

bool compare_string(comparison comp, char** arg1, char** arg2) {
    int comp_result = strcmp(*arg1, *arg2);
    switch (comp) {
        case GREATER:
            return comp_result > 0;
        case LESS:
            return comp_result < 0;
        case NEQUAL:
            return comp_result != 0;
        case EQUAL:
            return comp_result == 0;
        case GREATER_OR_EQUAL:
            return comp_result >= 0;
        case LESS_OR_EQUAL:
            return comp_result <= 0;
        default:
            return false;
    }
}

bool compare_num_types(columnType type, comparison comp, void* arg1, void* arg2) {
    switch (type) {
        case INTEGER:
            return compare_nums_int(comp, arg1, arg2);
        case BOOLEAN:
            return compare_nums_bool(comp, arg1, arg2);
        case FLOAT:
            return compare_nums_float(comp, arg1, arg2);
        default:
            return false;
    }
}

void* extract_value_address_from_literal_T(literal_T *lit) {
    switch (lit->type) {
        case LITERAL_TYPE__T_LIT_INTEGER_T:
            return &lit->value->num;
        case LITERAL_TYPE__T_LIT_STRING_T:
            return &lit->value->str;
        case LITERAL_TYPE__T_LIT_BOOLEAN_T:
            return &lit->value->boolean;
        case LITERAL_TYPE__T_LIT_FLOAT_T:
            return &lit->value->flt;
        default:
            return NULL;
    }
}

bool compare(data_iterator* iter, columnref_T* col, predicate_arg_T* arg, comparison cmp) {
    value columnValue;
    char buffer[256];
    char* bufferAddress = &buffer[0];
    columnValue.colChar = &bufferAddress;
    columnType colRefType = extract_column_value(iter, col, &columnValue);

    value argValue;
    char argBuffer[256];
    char* argBufferAddress = &argBuffer[0];
    argValue.colChar = &argBufferAddress;
    columnType argType;

    switch (arg->type) {
        case PREDICATE_ARG_TYPE__T_LITERAL_T:
            if (arg->arg->literal->type == LITERAL_TYPE__T_LIT_STRING_T) {
                return compare_string(cmp,
                                      columnValue.colChar,
                                      &arg->arg->literal->value->str);
            }
            return compare_num_types(colRefType,
                                   cmp,
                                   &columnValue,
                                   extract_value_address_from_literal_t(arg->arg->literal));
        case PREDICATE_ARG_TYPE__T_REFERENCE_T:
            argType = extract_column_value(iter, arg->arg->ref, &argValue);
            if (argType != colRefType) return false;
            return compare_num_types(argType, cmp, &columnValue, &argValue);
        default:
            return false;
    }
}


bool is_predicate_true(data_iterator* iter, predicate_T* pred) {
    if (!pred->__isset_arg) return true;

    if (pred->type == PREDICATE_TYPE__T_COMPARISON_T) {
        return compare(iter, pred->column, pred->arg, pred->cmp_type);
    }

    switch (pred->predicate_op) {
        case AND:
            return
            is_predicate_true(iter, (predicate_T*) pred->left->pdata[0])
            && is_predicate_true(iter, (predicate_T*) pred->right->pdata[0]);
        case OR:
            return
            is_predicate_true(iter, (predicate_T*) pred->left->pdata[0])
            || is_predicate_true(iter, (predicate_T*) pred->right->pdata[0]);
    }
    return false;
}

bool seek_next_predicate(data_iterator* iter, predicate_T* pred) {
    data_header d_header = {false};

    int32_t col_int;
    char* col_char;
    bool col_bool;
    double col_float;
    uint32_t next_page_number = iter->cur_page->header->next_page_ordinal;
    while (has_next(iter) || next_page_number != 0) {
        do {
            if (iter->rows_read_on_page > 0) {
                iter->ptr += iter->tb->header->one_row_size;
            }
            d_header = get_data_header(iter);
            iter->rows_read_on_page += 1;
        } while (has_next(iter) && !d_header.valid);

        if (d_header.valid) {
            bool predicate_true = is_predicate_true(iter, pred);
            if (predicate_true) return true;
            continue;
        }

        if (iter->cur_page != iter->tb->first_page && iter->cur_page != iter->tb->first_available_for_write_page) {
            close_page(iter->cur_page);
        }
        if (next_page_number == 0) {
            break;
        }
        iter->cur_page = read_page(iter->db, next_page_number);
        next_page_number = iter->cur_page->header->next_page_ordinal;
        iter->rows_read_on_page = 0;
        iter->ptr = 0;
    }
    return false;
}

dataHeader get_data_header(data_iterator* iter) {
    return *((dataHeader*)((char*) iter->currentPage->bytes + iter->pointer));
}

bool get_custom(data_iterator* iter, const char* columnName, void* dest) {
    for (int i = 0; i < iter->tb->header->columnAmount; i++) {
        columnHeader currentColumn = iter->tb->header->columns[i];
        if (strcmp(columnName, currentColumn.columnName) == 0) {
            switch (currentColumn.type) {
                case INTEGER:
                    return get_integer(iter, columnName, dest);
                case STRING:
                    return get_string(iter, columnName, (char **) &dest);
                case BOOLEAN:
                    return get_bool(iter, columnName, dest);
                case FLOAT:
                    return get_float(iter, columnName, dest);
                default:
                    return false;
            }
        }
    }
    return false;
}

bool get_integer(data_iterator* iter, const char* columnName, int32_t* dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, INTEGER);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    *dest = *((int32_t*)((char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData));
    return true;
}

bool get_string(data_iterator* iter, const char* columnName, char** dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, STRING);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    strcpy(*dest, (char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData);
    return true;
}

bool get_bool(data_iterator* iter, const char* columnName, bool* dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, BOOLEAN);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    *dest = *((bool*)((char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData));
    return true;
}

bool get_float(data_iterator* iter, const char* columnName, double* dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, FLOAT);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    *dest = *((double *)((char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData));
    return true;
}

uint16_t delete_where(database* db, table* tb, const char* colName, columnType colType, const void* val) {
    data_iterator* iter = initialize_iterator(db, tb);
    uint16_t removedObjects = 0;

    while (seek_next_where(iter, colName, colType, val)) {
        page* pg = iter->currentPage;
        void* pageBytes = pg->bytes;
        dataHeader header = get_data_header(iter);
        header.is_valid = false;
        memcpy(pageBytes + iter->pointer, &header, sizeof(dataHeader));
        write_page_to_file(pg, db->file);
        removedObjects++;
    }
    free(iter);
    return removedObjects;
}

uint16_t update_where(database* db, table* tb,
                     const char* whereColName, columnType whereColType, const void* whereVal,
                     const char* updateColName, columnType updateColType, const void* updateVal) {
    data_iterator* iter = initialize_iterator(db, tb);
    uint16_t updatedObjects = 0;
    while (seek_next_where(iter, whereColName, whereColType, whereVal)) {
        page* pg = iter->currentPage;
        void* pageBytes = pg->bytes;
        int32_t columnDataOffset = get_offset_to_column_data(iter, updateColName, updateColType);
        columnHeader* c_header = get_column_header_by_name(iter->tb, updateColName);
        memcpy(pageBytes + iter->pointer + columnDataOffset, updateVal, c_header->size);
        write_page_to_file(pg, db->file);
        updatedObjects++;
    }
    free(iter);
    return updatedObjects;
}

uint16_t update_where_predicate(database* db, table* tb, predicate_T* pred, GPtrArray *sets) {
    data_iterator* iter = initialize_iterator(db, tb);
    uint16_t updated_objects = 0;
    while (seek_next_predicate(iter, pred)) {
        page* pg = iter->cur_page;
        void* page_bytes = pg->bytes;
        for (size_t i = 0; i < sets->len; i++) {
            set_value_T *upd = (set_value_T*) g_ptr_array_index(sets, i);
            column_type type = discover_column_type_by_name(iter, upd->col->col_name);
            int32_t column_data_offset = get_offset_to_column_data(iter, upd->col->col_name, type);
            columnHeader* c_header = get_column_header_by_name(iter->tb, upd->col->col_name);
            memcpy(page_bytes + iter->ptr + column_data_offset,
                   extract_value_address_from_literal_t(upd->lit),
                   c_header->size);
            updated_objects++;
        }
        write_page_to_file(pg, db->file);
    }
    free(iter);
    return updated_objects;
}

uint16_t delete_where_predicate(database* db, table* tb, predicate_T* pred) {
    data_iterator* iter = initialize_iterator(db, tb);
    uint16_t removed_objects = 0;

    while (seek_next_predicate(iter, pred)) {
        page* pg = iter->cur_page;
        void* page_bytes = pg->bytes;
        data_header header = get_data_header(iter);
        header.valid = false;
        memcpy(page_bytes + iter->ptr, &header, sizeof(data_header));
        write_page_to_file(pg, db->file);
        removed_objects++;
    }
    free(iter);
    return removed_objects;
}


void print_join_table(database* db, table* tb1, table* tb2, const char* onColumnT1, const char* onColumnT2, columnType type) {

    data_iterator* iter1 = initialize_iterator(db,tb1);
    data_iterator* iter2 = initialize_iterator(db, tb2);
    while (seek_next(iter1)) {
        columnHeader* onColumnT1Header = get_column_header_by_name(tb1, onColumnT1);
        void* value = malloc(onColumnT1Header->size);
        get_custom(iter1, onColumnT1, value);
        while (seek_next_where(iter2, onColumnT2, type, value)) {
            for (int i = 0; i < iter1->tb->header->columnAmount; i++) {
                columnHeader cheader = iter1->tb->header->columns[i];
                void* curColumnValue = malloc(cheader.size);
                get_custom(iter1, cheader.columnName, curColumnValue);
                switch (cheader.type) {
                    case INTEGER:
                        printf("\t %d \t", *((int32_t*) curColumnValue));
                        break;
                    case STRING:
                        printf("\t%s\t", (char*) curColumnValue);
                        break;
                    case BOOLEAN:
                        printf("\t%d\t", *((bool*) curColumnValue));
                        break;
                    case FLOAT:
                        printf("\t%f\t", *((float*) curColumnValue));
                        break;
                }
            }

            for (int i = 0; i < iter2->tb->header->columnAmount; i++) {
                columnHeader cheader = iter2->tb->header->columns[i];
                void* curColumnValue = malloc(cheader.size);
                get_custom(iter2, cheader.columnName, curColumnValue);
                switch (cheader.type) {
                    case INTEGER:
                        printf("\t%d\t", *((int32_t*) curColumnValue));
                        break;
                    case STRING:
                        printf("\t%s\t", (char*) curColumnValue);
                        break;
                    case BOOLEAN:
                        printf("\t%d\t", *((bool*) curColumnValue));
                        break;
                    case FLOAT:
                        printf("\t%f\t", *((float*) curColumnValue));
                        break;
                }
            }

            printf("\n");
        }
        iter2 = initialize_iterator(db, tb2);
    }
    free(iter1);
    free(iter2);

}
