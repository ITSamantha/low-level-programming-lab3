#ifndef LLP_LAB3_T_CLIENT_SERIALIZER_H
#define LLP_LAB3_T_CLIENT_SERIALIZER_H

#include "../gen-c_glib/structs_types.h"
#include "../client/data.h"

statement_T* t_serialize_statement(statement*);

#endif //LLP_LAB3_T_CLIENT_SERIALIZER_H
